$(document).ready(function () {

    $("input[type=radio]").change(function () {
        var image_name = $(this).attr("id").split("-")[1];
        var image_extension = ".jpg";
        console.log("changing to " + image_name);

        if (image_name.indexOf("6") !== -1) {
            image_extension = ".gif";
        }

        $('#background').attr("src", "../images/" + image_name + image_extension);
    });


    $(".dolphin").hide();

    $("input[type=checkbox]").attr("checked", false);

    $("input[type=checkbox]").change(function () {
        var theId = "#" + $(this).attr("id").split("-")[1];

        if ($(this).is(":checked")) {
            $(theId).show();
        } else {
            $(theId).hide();
        }
    });


    /* 0 :: vertical (px)            1 :: horizontal (%) */
    var dolphinPos = {
        "dolphin1": [300, 1],
        "dolphin2": [350, 70],
        "dolphin3": [150, 34],
        "dolphin4": [100, 60],
        "dolphin5": [250, 10],
        "dolphin6": [190, 50],
        "dolphin7": [180, 85],
        "dolphin8": [200, 10]
    };


    $("#vertical-control").change(function () {

        var magnitude = (50 - $(this).val()) * 5;

        for (var image in dolphinPos) {
            $("#" + image).css("top", dolphinPos[image][0] + magnitude + "px");
        }


    });

    $("#horizontal-control").change(function () {

        var magnitude = (50 - $(this).val());
        console.log("changing to" + image);
        for (var image in dolphinPos) {
            $("#" + image).css("left", dolphinPos[image][0] + magnitude + "%");

        }

        $("#size-control").change(function () {

            for (var image in dolphinPos) {
                $("#" + image).resize(image.height);
            }


            $(function () {
                var el, newPoint, newPlace, offset;

                // Select all range inputs, watch for change
                $("input[type='range']").change(function () {

                    // Cache this for efficiency
                    el = $(this);

                    // Measure width of range input
                    width = el.width();

                    // Figure out placement percentage between left and right of input
                    newPoint = (el.val() - el.attr("min")) / (el.attr("max") - el.attr("min"));

                    // Janky value to get pointer to line up better
                    offset = -1.3;

                    // Prevent bubble from going beyond left or right (unsupported browsers)
                    if (newPoint < 0) {
                        newPlace = 0;
                    }
                    else if (newPoint > 1) {
                        newPlace = width;
                    }
                    else {
                        newPlace = width * newPoint + offset;
                        offset -= newPoint;
                    }

                    // Move bubble
                    el
                        .next("output")
                        .css({
                            left: newPlace,
                            marginLeft: offset + "%"
                        })
                        .text(el.val());
                })
                // Fake a change to position bubble at page load
                    .trigger('change');
            });


        });

    });
});
